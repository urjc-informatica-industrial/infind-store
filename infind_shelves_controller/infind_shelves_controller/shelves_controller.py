from threading import Thread
from datetime import datetime
import os
import sys
import signal
from rclpy import qos
import json
from dataclasses import dataclass, field

from infind_store_msgs.srv import DeliverOrder, TakeProduct, TasksCompleted
from nav_msgs.msg import Odometry
from rosgraph_msgs.msg import Clock
from builtin_interfaces.msg import Time

import rclpy
from rclpy.node import Node
import sqlite3

ROS_CLOCK_MAX = int(os.getenv('ROS_CLOCK_MAX', "10000"))

try:
    import pandas as pd
    USE_PANDAS=True
except ImportError:
    USE_PANDAS=False 

try:
    import ttkbootstrap as ttk
    from .shelves_controller_ui import ShelvesControllerUI
except Exception as e:
    print('No ttk available, imposible to run inteface', e)


@dataclass
class Point:
    """Location point"""
    x: float
    y: float

@dataclass
class Shelve:
    """Shelve"""
    name: str
    id: int
    location: Point

@dataclass
class Product:
    id: int
    name: str
    code: str
    units: int = 0    
    
    def __lt__(self, other):
        return self.id < other.id
    
@dataclass
class Order:
    id: int
    products: list[Product] = field(default_factory=list)
    
    def isEqual(self, other) -> bool:
        self.products.sort()
        other.products.sort()
        return self == other

    def __lt__(self, other):
        return self.id < other.id
    
    def toString(self):
        output = f"Order: {self.id}\n"
        for product in self.products:
            output += f"\n id: {product.id}, code: {product.code}, name: {product.name}, units: {product.units}"

        return output


@dataclass
class ShoppingCart:
    products: list[Product] = field(default_factory=list)
    
    def add(self, id, name, code, units):
        for p in self.products:
            if p.id == id:
                p.units += units
                return
            
        self.products.append(Product(id, name, code, units))    
        

class Database():
    def __init__(self, database_path):
        self.con = sqlite3.connect(database_path)
        self.con.cursor().execute('PRAGMA journal_mode=WAL')

    def __del__(self):
        self.con.close()
    
    def getAllOrders(self) -> Order:
        cur = self.con.cursor()
        cur.execute("""
                    SELECT orderNumber FROM DELIVERY_ORDER
                    """)
        orders = []
        for row in cur.fetchall():
            order_id = row[0]
            orders.append(self.getOrder(order_id))
        return orders
        
    def getOrder(self, orderId) -> Order:
        cur = self.con.cursor()
        cur.execute("""
                    SELECT p.id, p.productName, p.productCode, li.quantity 
                    FROM LINE_ITEM li 
                    LEFT JOIN PRODUCT p on p.id = li.productId 
                    WHERE orderNumber = ?
                    """, (orderId,))
        order = Order(id=orderId)
        for row in cur.fetchall():
            order.products.append(Product(row[0], row[1], row[2], row[3]))
        return order

    def getAllShelvesByName(self) -> dict[str, Shelve]:
        cur = self.con.cursor()
        cur.execute("SELECT shelfName, id, locationX, locationY FROM STORAGE_SHELF")
        shelves = {}
        for row in cur.fetchall():
            shelves[row[0]] = Shelve(name=row[0], id=row[1], location=Point(x=row[2], y=row[3]))
        return shelves
        
    def getProductByCode(self, product_code) -> Product:
        cur = self.con.cursor()
        cur.execute("""
            SELECT id, productName, productCode FROM PRODUCT WHERE productCode = ?
            """, (product_code,))
        result = cur.fetchone()
        if result:
            row = result
            return Product(row[0], row[1], row[2])
        else:
            return None

    def getProductStockInShelft(self, product_code, shelf_id):
        cur = self.con.cursor()
        cur.execute("""
            SELECT si.quantity FROM STORAGE_ITEM si LEFT JOIN PRODUCT p on si.productId = p.id  WHERE productId = (
                SELECT id FROM PRODUCT WHERE productCode = ?
            ) AND storageShelfId = ?
            """, (product_code, shelf_id))
        result = cur.fetchone()
        if result:
            return result[0]
        else:
            return 0
        
    def reduceProductStockInShelft(self, product_code, shelf_id, quantity):
        cur = self.con.cursor()
        cur.execute("""
            UPDATE STORAGE_ITEM SET quantity = quantity - ? WHERE productId = (
                SELECT id FROM PRODUCT WHERE productCode = ?
            ) AND storageShelfId = ?
            """, (quantity, product_code, shelf_id))
        self.con.commit()


class ShelvesControllerService(Node):
    def __init__(self, ui):
        super().__init__('shelves_service')
        self._keep_running = True
        self.ui = ui
        self.robot_position = Point(x=0, y=0)

        if USE_PANDAS:
            self.df = pd.DataFrame()

        db_path = os.getenv('DB_FILE')
        self.db = Database(db_path)
        self.shelves = self.db.getAllShelvesByName()
        self.shopping_cart = ShoppingCart()
        self.attended_orders = []
        self.clock = None

        self.deliver_order_srv = self.create_service(DeliverOrder, 
                                                     'store/deliver_order', self.deliver_order)
        self.take_products_srv = self.create_service(TakeProduct, 
                                                     'store/take_products', self.take_products)
        self.tasks_completed_srv = self.create_service(TasksCompleted, 
                                                      'store/tasks_completed', self.tasks_completed)
        
        self.clock_sub = self.create_subscription(
            Clock,
            '/clock',
            self.update_clock,
            qos_profile=qos.qos_profile_system_default
        )
        
        self.odom_sub = self.create_subscription(Odometry, '/odom', self.update_robot_position, qos_profile=qos.qos_profile_system_default)
        # self.srv = self.create_service(AddTwoInts, 'add_two_ints', self.add_two_ints_callback)

    @property
    def keep_running(self):
        return self._keep_running


    def update_clock(self, msg: Clock):
        self.clock = msg.clock
        if self.clock.sec > ROS_CLOCK_MAX:
            self.get_logger().info('Max time overdue. Finish')
            self.finish()

    def deliver_order(self, request: DeliverOrder.Request, response: DeliverOrder.Response):
        response.accepted = False
        
        if self.distance_from_robot_to_shelf("Entregas") > 0.5:
            self.get_logger().info("Robot is far from the delivery zone")
            return response
        
        self.get_logger().info(f"Request to Deliver order: {request.order.order_id}")
        for line in request.order.lines:
            self.get_logger().info(f"Line: {line.units} units - {line.product.code}")
        
        order = self.db.getOrder(request.order.order_id)
        
        req_order = Order(id=request.order.order_id)
        for line in request.order.lines:
            product = self.db.getProductByCode(line.product.code)
            product.units = line.units
            req_order.products.append(product)
            
        # Virtual order created with shopping cart products
        sc_order = Order(id=request.order.order_id)
        for item in self.shopping_cart.products:
            product = Product(item.id, item.name, item.code, item.units)
            sc_order.products.append(product)
            
        if order.isEqual(req_order):
            if order.isEqual(sc_order):
                response.accepted = True
                self.get_logger().info(f"Delivery order {order.id} completed. Added to attended orders.")
                self.attended_orders.append(order)
                self.get_logger().info("Cleaning shopping cart.")
                self.shopping_cart.products.clear()
                cart = [['-', '-', '-', '-']]
                self.ui['instance'].shopping_cart_frame.updateShoppingCart(cart)
                self.ui['instance'].stats_frame.update_value(len(self.attended_orders))
            else:
                self.get_logger().info("Products in shopping cart don't match order products")
        else:
            self.get_logger().info(f"Products order {order.id} don't match request order")
            self.get_logger().info("order")
            self.get_logger().info(order.toString())
            self.get_logger().info("req_order")
            self.get_logger().info(req_order.toString())
        
        return response

    def take_products(self, request: TakeProduct.Request, response: TakeProduct.Response):
        self.get_logger().info("Take products")
        # shelft_id = request.
        response.given_units = 0

        if self.distance_from_robot_to_shelf(request.shelf.name) < 0.5:
            shelf = self.shelves[request.shelf.name]                
            product_code = request.product.code
            self.get_logger().info(f"Query {product_code} in {shelf.id} ({shelf})")
            stock = self.db.getProductStockInShelft(product_code, shelf.id)
            self.get_logger().info(f"Stock is {stock}")
            if stock >= request.units:
                self.db.reduceProductStockInShelft(product_code, shelf.id, request.units)
                response.given_units = request.units
                product = self.db.getProductByCode(product_code)
                self.shopping_cart.add(product.id, product.name, product.code, request.units)
                self.updateShoppingCartView()
                self.get_logger().info(f"Serving products [{product_code}], updating stock in shelf {request.shelf.name}")
            else:
                self.get_logger().info(f"Missing stock for product [{product_code}] in shelf {request.shelf.name}")
        else:
            self.get_logger().info(f"The robot is far from {request.shelf.name} it can not ask for products")
        return response

    def tasks_completed(self, request: TasksCompleted.Request, response: TasksCompleted.Response):
        response.accepted = False
        allOrders = self.db.getAllOrders()
        allOrders.sort()        
        self.attended_orders.sort()

        self.get_logger().info("Attended orders")
        for order in self.attended_orders:
            order.products.sort()
            self.get_logger().info(order.toString())    

        self.get_logger().info("Orders in database")
        for order in allOrders:
            order.products.sort()
            self.get_logger().info(order.toString())
        
        if allOrders == self.attended_orders:
            response.accepted = True
            self.get_logger().info("All the orders have been attended. Congratulation!")
        else:
            self.get_logger().info("There are some orders missings")

        self.finish()
            
        # Check if all orders have been attended.
        return response
    
    def update_robot_position(self, message: Odometry):
        self.robot_position.x = message.pose.pose.position.x
        self.robot_position.y = message.pose.pose.position.y

        if USE_PANDAS and self.clock:
            # self.get_logger().info('Update position')
            position = message.pose.pose.position
            metric = {
                'time': datetime.now(),
                'rostime': self.clock.sec,
                'x': position.x,
                'y': position.y,
            }
            self.df = self.df.append(metric, ignore_index=True) 

    def distance_from_robot_to_shelf(self, shelfName):
        print(shelfName)
        shelf_pos : Point = self.shelves[shelfName].location
        x_s = shelf_pos.x
        y_s = shelf_pos.y
        x_r = self.robot_position.x
        y_r = self.robot_position.y
        return ((x_r - x_s)**2 + (y_r - y_s)**2)**0.5
    
    def updateShoppingCartView(self):
        cart = [[product.name, product.units, product.id, product.code] for product in self.shopping_cart.products]

        self.ui['instance'].shopping_cart_frame.updateShoppingCart(cart)

    def finish(self):
        self.destroy_subscription(self.odom_sub)
        self.destroy_subscription(self.clock_sub)
        if USE_PANDAS:
            path=os.getenv('DF_STORE_PATH', '~/debug.pickle')
            self.df.to_pickle(path)
            print('Save pandas dataframe')
        self._keep_running = False


def exit_gracefully(signum, _kiframe):
    print('captured signal %d' % signum)
    raise(SystemExit)


class DumbAttribute:
    def __init__(self, name):
        self.name = name
    def setValue(self, value):
        print(f'{self.name}: Got {value}')
    def updateShoppingCart(self, value):
        pass
    def update_value(self, value):
        pass


class DumbUIFrame:
    def __getattribute__(self, name: str):
        return DumbAttribute(name)


def headless():
    signal.signal(signal.SIGINT, exit_gracefully)
    signal.signal(signal.SIGTERM, exit_gracefully)
    
    rclpy.init()

    dumb = {
        'instance': DumbUIFrame()
    }

    shelves_service = ShelvesControllerService(dumb)
    shelves_service.get_logger().info('Service started')
    shelves_service.get_logger().info('Headless started')

    while shelves_service.keep_running:
        rclpy.spin_once(shelves_service)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    shelves_service.destroy_node()
    rclpy.shutdown()


def main(args=None):
    rclpy.init(args=args)

    ui = {
        'instance': None
    }

    def createUI():
        app = ttk.Window(
            title="Shelves Controller",
            themename="flatly",
            size=(700, 500),
            resizable=(False, False),
        )
        ui['instance'] = ShelvesControllerUI(app)

        def on_closing():
            app.destroy()
            rclpy.shutdown()
            exit()

        app.protocol("WM_DELETE_WINDOW", on_closing)

        app.mainloop()
    
    t = Thread(target=createUI, args=[])
    t.start()

    shelves_service = ShelvesControllerService(ui)
    shelves_service.get_logger().info('Service started')
    shelves_service.get_logger().info('UI started')

    rclpy.spin(shelves_service)
    shelves_service.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
