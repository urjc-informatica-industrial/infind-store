import ttkbootstrap as ttk
from ttkbootstrap.constants import *
from ttkbootstrap.tooltip import ToolTip
from ttkbootstrap.toast import ToastNotification
import tksheet
import subprocess

class ShelvesControllerUI(ttk.Frame):
    
    def __init__(self, master, **kwargs):
        super().__init__(master, padding=(10, 10), **kwargs)        
        self.notebook = ttk.Notebook(self)

        # Crear el contenido de cada una de las pestañas.
        self.shopping_cart_container = ttk.Frame(self)
        self.shopping_cart_frame = ShoppingCart(self.shopping_cart_container)
        self.shopping_cart_frame.pack(fill='both', expand=True)
        
        self.tools_container = ttk.Frame(self)
        self.tools_frame = ToolsButtons(self.tools_container)
        self.tools_frame.pack(fill='both', expand=True)

        self.stats_container = ttk.Frame(self)
        self.stats_frame = StatsFrame(self.stats_container)
        self.tools_frame.pack(fill='both', expand=True)
        
        # Añadirlas al panel con su respectivo texto.
        self.notebook.add(self.shopping_cart_container, text="Shopping Cart", padding=20)
        self.notebook.add(self.tools_container, text="Tools", padding=20)
        self.notebook.add(self.stats_container, text="Stats", padding=20)

        self.notebook.pack(padx=5, pady=5, fill='both', expand=True)
        self.pack(fill='both', expand=True)

class StatsFrame(ttk.Frame):
    def __init__(self, master):
        super().__init__(master)

        container = ttk.Frame(master)
        
        # Crear el label y el valor
        self.label = ttk.Label(container, text="Pedidos servidos: ")
        self.value = ttk.Label(container, text="0")
        
        # Posicionar el label y el valor
        self.label.grid(row=0, column=0, sticky="w")
        self.value.grid(row=0, column=1, sticky="e")

        container.pack()
        self.pack()
        
    def update_value(self, new_value):
        self.value.config(text=new_value)

class ShoppingCart(ttk.Frame):
    def __init__(self, master):
        super().__init__(master)

        self.sheet = tksheet.Sheet(master)
        self.sheet.grid()
        self.sheet.headers(newheaders=["Product", "Units", "ID", "Code"])


        self.sheet.set_sheet_data([['-', '-', '-', '-']])

        self.sheet.enable_bindings(("single_select",
                       "row_select",
                       "column_width_resize",
                       "arrowkeys",
                       "right_click_popup_menu",
                       "copy",
                       "cut",
                       "paste"))

        self.sheet.pack(fill='both', expand=True)

    def updateShoppingCart(self, values):
        self.sheet.set_sheet_data(values)

class ToolsButtons(ttk.Frame):
    def __init__(self, master):
        super().__init__(master)

        container = ttk.Frame(master)

        self.move1 = ttk.Button(container, bootstyle="outline", text="Mover a 1",
                                command=lambda: self.go_to_shelf("Estantería 1"))
        self.move1.grid(row=0, column=0, padx=5, pady=5)
        self.move2 = ttk.Button(container, bootstyle="outline", text="Mover a 2",
                                command=lambda: self.go_to_shelf("Estantería 2"))
        self.move2.grid(row=0, column=1, padx=5, pady=5)
        self.move3 = ttk.Button(container, bootstyle="outline", text="Mover a 3",
                                command=lambda: self.go_to_shelf("Estantería 3"))
        self.move3.grid(row=0, column=2, padx=5, pady=5)
        self.move4 = ttk.Button(container, bootstyle="outline", text="Mover a 4",
                                command=lambda: self.go_to_shelf("Estantería 4"))
        self.move4.grid(row=0, column=3, padx=5, pady=5)
        self.move5 = ttk.Button(container, bootstyle="outline", text="Mover a 5",
                                command=lambda: self.go_to_shelf("Estantería 5"))
        self.move5.grid(row=1, column=0, padx=5, pady=5)
        self.move6 = ttk.Button(container, bootstyle="outline", text="Mover a 6",
                                command=lambda: self.go_to_shelf("Estantería 6"))
        self.move6.grid(row=1, column=1, padx=5, pady=5)
        self.move7 = ttk.Button(container, bootstyle="outline", text="Mover a 7",
                                command=lambda: self.go_to_shelf("Estantería 7"))
        self.move7.grid(row=1, column=2, padx=5, pady=5)
        self.move8 = ttk.Button(container, bootstyle="outline", text="Mover a 8",
                                command=lambda: self.go_to_shelf("Estantería 8"))
        self.move8.grid(row=1, column=3, padx=5, pady=5)
        
        self.moveDeliver = ttk.Button(container, bootstyle="outline", text="Ir a entrega",
                                      command=lambda: self.go_to_shelf("Entregas"))
        self.moveDeliver.grid(row=2, column=1, padx=5, pady=5)
        self.moveExit = ttk.Button(container, bootstyle="outline", text="Ir a salida",
                                   command=lambda: self.go_to_shelf("Salida"))
        self.moveExit.grid(row=2, column=2, padx=5, pady=5)

        container.pack()

        second_container = ttk.Frame(master)
        self.move1 = ttk.Button(second_container, bootstyle="outline", text="Regenerar DB",
                                command=lambda: self.run_command("node ~/tools/infind-cmd/infind.mjs db"))
        self.move1.grid(row=0, column=0, padx=5, pady=5)
        second_container.pack()

        self.pack()
        
    def go_to_shelf(self, id):
        shelves = {
                    "Entregas":	(0.0, 0.0),
                    "Estantería 1": (-1.881203, 5.053046),
                    "Estantería 2": (1.680894, 5.097794),
                    "Estantería 3": (4.099954, 1.724051),
                    "Estantería 4": (4.259444, -1.885899),
                    "Estantería 5": (1.982538, -4.978827),
                    "Estantería 6": (-1.636198, -5.043471),
                    "Estantería 7": (-4.301246, -1.346546),
                    "Estantería 8": (-4.189356, 2.01992),
                    "Salida": (-4.845454, -4.895001)
                    }
        item = shelves[id]
        x = str(item[0])
        y = str(item[1])
        cmd1 = "ros2 service call /gazebo/set_entity_state gazebo_msgs/SetEntityState 'state: {name: waffle_pi, pose: {position:{x: " + x + ", y: " + y + ", z: 0.0}}, reference_frame: world}'; "
        cmd2 = "ros2 topic pub -1 /initialpose geometry_msgs/PoseWithCovarianceStamped '{ header: {stamp: {sec: 0, nanosec: 0}, frame_id: \"map\"}, pose: { pose: {position: {x: " + x + ", y: " + y + ", z: 0.0}, orientation: {w: 1}}, } }';"
        cmds = cmd1 + cmd2
        self.run_command(cmds)
        
        
    def run_command(self, subcommand):
        command = f". ~/.bashrc_infind; cd ~/project3_ws; . ./install/setup.sh; {subcommand}"
        result = subprocess.run(['/bin/bash', '-c', command], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        output = result.stdout.decode('utf-8')
        output_error = result.stderr.decode('utf-8')
        print(output[:10000])
        print(output_error[:10000])
        
        