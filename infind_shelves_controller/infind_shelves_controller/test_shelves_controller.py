import pytest

from .shelves_controller import Product, Order

def test_product_comparison():
    assert Product(1, 'aa', 'cc', 12) == Product(1, 'aa', 'cc', 12)
    assert Product(1, 'aa', 'cc', 12) != Product(1, 'aa', 'cc', 13)
    assert Product(1, 'aa', 'cc', 12) != Product(4, 'aa', 'cc', 12)
    
def test_order_comparison():
    ord1 = Order(12)
    ord1.products.append(Product(1, 'aa', 'cc', 12))
    ord1.products.append(Product(2, 'bb', 'cc', 2))
    
    ord2 = Order(12)
    ord2.products.append(Product(1, 'aa', 'cc', 12))
    ord2.products.append(Product(2, 'bb', 'cc', 2))
    
    assert ord1.isEqual(ord2)
    
    ord2 = Order(12)
    ord2.products.append(Product(2, 'bb', 'cc', 2))
    ord2.products.append(Product(1, 'aa', 'cc', 12))
    
    assert ord1.isEqual(ord2)
    
    ord2 = Order(12)
    ord2.products.append(Product(2, 'bb', 'cc', 3))
    ord2.products.append(Product(1, 'aa', 'cc', 12))
    
    assert not ord1.isEqual(ord2)
    
    ord2 = Order(12)
    ord2.products.append(Product(2, 'bb', 'cc', 2))
    ord2.products.append(Product(1, 'aa', 'cc', 12))
    ord2.products.append(Product(3, 'aa', 'cc', 12))
    
    assert not ord1.isEqual(ord2)
    
    