from setuptools import setup

package_name = 'infind_shelves_controller'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='diegogd',
    maintainer_email='diego.gdiaz@urjc.es',
    description='TODO: Package description',
    license='MIT',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'shelves_controller = infind_shelves_controller.shelves_controller:main',
            'shelves_controller_headless = infind_shelves_controller.shelves_controller:headless'
        ],
    },
)
